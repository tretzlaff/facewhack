For each round of the game you will first be shown the target face you are looking for, then a grid of faces to select from. 

If select the right face from the grid you will be awarded a round score and shown a win screen.

The round score is the remaining milliseconds multiplied by the round number.

On the win screen you will be shown the total score, and rounds completed. Tapping on this screen starts the next round. 

Each round you will have less time to select the right face from the grid.

If you select the wrong face from the grid you will be shown the lose screen, total rounds completed and final score.

Clicking on the lose screen will reset and restart (timers and score) the game.
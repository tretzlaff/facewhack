package com.mygdx.facewhack;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyGdxGameFaceWhack extends ApplicationAdapter {
	SpriteBatch batch;
	Music music;
    Sound win;
    Sound lose;
    TextureRegion startBanner;
    TextureRegion loseBanner;
    TextureRegion winBanner;
    BitmapFont font;
    List<Face> faces = new ArrayList<Face>();
    Face answerFace;
    GameState gameState;
    long answerTime;// starts at 5000 (milliseconds) decrements by 100 per round
    long answerTimeStart;
    long answerTimeEnd;
    long remaining;
    long showTime;
    long gameScore;
    int roundCounter;
    int answerIndex;
    int width;
    int height;
    int centerx;
    int centery;
    int bannerx;
    int bannery;
    int answerx;
    int answery;
    int rya;
    int ryb;
    int ryc;
    int cxa;
    int cxb;
    int cxc;
    int targetlowx;
    int targetlowy;
    int targethighx;
    int targethighy;

	@Override
	public void create () {
        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();
        centerx = width/2;
        centery = height/2;
        bannerx = centerx -180;
        bannery = centery - 120;
        answerx = centerx - 73;
        answery = centery - 73;
        rya = centery + 73;
        ryb = centery - 73;
        ryc = centery -219;
        cxa = centerx - 219;
        cxb = centerx - 73;
        cxc = centerx + 73;
        roundCounter = 0;
        answerTime = 5000;
        showTime = 2500;

        startBanner = new TextureRegion(new Texture("start.png"));
        loseBanner = new TextureRegion(new Texture("loser.png"));
        winBanner = new TextureRegion(new Texture("weiner.png"));
		batch = new SpriteBatch();
        font = new BitmapFont(Gdx.files.internal("arial.fnt"));
        font.setColor(255,0,0,255);
        faces.add(new Face(0,0,new TextureRegion(new Texture("f1.png")),0));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f2.png")),1));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f3.png")),2));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f4.png")),3));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f5.png")),4));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f6.png")),5));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f7.png")),6));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f8.png")),7));
        faces.add(new Face(0,0,new TextureRegion(new Texture("f9.png")),8));

        music = Gdx.audio.newMusic(Gdx.files.internal("YodasTheme.mp3"));
        music.setLooping(true);
        music.play();
        win = Gdx.audio.newSound(Gdx.files.internal("Lightsaber.mp3"));
        lose = Gdx.audio.newSound(Gdx.files.internal("Blaster-Imperial.wav"));
        gameState = GameState.Begin;
        setUpRound();
	}

	@Override
	public void render () {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //Gamestate switcher
        //gamestateBegin - if touched change game state to show answer
        if(gameState == GameState.ShowWin){
            //gamestateRoundWin - congratulate on win until touch, then decrement time counter and move to next round
            if(Gdx.input.justTouched()) {
                setUpRound();
                gameState = GameState.ShowAnswer;
            }else{
                batch.begin();
                batch.draw(winBanner,bannerx,bannery);
                font.draw(batch,"Tap screen to begin next round",50,50);
                font.draw(batch,"Game score: " + gameScore,50,100);
                font.draw(batch,"Round counter: " + roundCounter,50,150);
                batch.end();
            }
        }else if(gameState == GameState.ShowLose){
            //gamestateRoundLose - condolences on loss and touch to restart
            if(Gdx.input.justTouched()) {
                resetGameCounters();
                setUpRound();
                gameState = GameState.ShowAnswer;
            }else{
                batch.begin();
                batch.draw(loseBanner,bannerx,bannery);
                font.draw(batch,"Tap screen to begin a new game",50,50);
                font.draw(batch,"Final score: " + gameScore,50,100);
                font.draw(batch,"Round counter: " + (roundCounter -1),50,150);
                batch.end();
            }
        }else if(gameState == GameState.GetAnswer){
            //gamestateGetAnswer - showChoiceGrid for x seconds, starting with 5, decrementing by half seconds
            //if touched and right change game state to round win, if touched and wrong or time runs out, show roundlose
            if(Gdx.input.justTouched()) {
                int touchx = Gdx.input.getX();
                int touchy = height - Gdx.input.getY();
                if(touchx >= targetlowx && touchx <= targethighx && touchy >= targetlowy && touchy <= targethighy ){//coordinates of correct answer and touchPos intersect
                    gameState = GameState.ShowWin;
                    //gameScore calculations
                    remaining = answerTimeEnd - TimeUtils.millis();
                    gameScore = gameScore + remaining * roundCounter;
                    answerTime = answerTime - 200;
                    win.play();
                }else{
                    gameState = GameState.ShowLose;
                    //high score handling - getting cannot resolve symbol on the method local looking into gradle build files
                    //fh = new Gdx.files.local("data/highScore.txt");
                    lose.play();
                }
            }else if(TimeUtils.millis() > answerTimeEnd ){
                gameState = GameState.ShowLose;
                lose.play();
            }else{
                batch.begin();
                batch.draw(faces.get(0).image,cxa,rya);
                batch.draw(faces.get(1).image,cxb,rya);
                batch.draw(faces.get(2).image,cxc,rya);
                batch.draw(faces.get(3).image,cxa,ryb);
                batch.draw(faces.get(4).image,cxb,ryb);
                batch.draw(faces.get(5).image,cxc,ryb);
                batch.draw(faces.get(6).image,cxa,ryc);
                batch.draw(faces.get(7).image,cxb,ryc);
                batch.draw(faces.get(8).image,cxc,ryc);
                //show countdown
                remaining = answerTimeEnd - TimeUtils.millis();
                font.draw(batch,"Milliseconds remaining: " + remaining,50,50);
                batch.end();
            }
        }else if(gameState == GameState.ShowAnswer){
            //gamestateShowAnswer - once time is up change to get answer
            if(Gdx.input.justTouched()) {
                roundCounter++;
                answerTimeStart= TimeUtils.millis();
                answerTimeEnd = answerTimeStart + answerTime;
                gameState = GameState.GetAnswer;
            }else{
                batch.begin();
                font.draw(batch,"Tap screen to show the answer grid.",50,50);
                batch.draw(faces.get(answerIndex).image,answerx,answery);
                batch.end();
            }
        }else if(gameState == GameState.Begin){
            if(Gdx.input.justTouched()) {
                gameState = GameState.ShowAnswer;
            }else{
                batch.begin();
                batch.draw(startBanner,bannerx,bannery);
                batch.end();
            }
        }
	}

    private void resetGameCounters() {
        roundCounter=0;
        gameScore = 0;
        answerTime = 5000;
        showTime = 2000;
    }

    @Override
	public void dispose () {
		batch.dispose();
        music.dispose();
        win.dispose();
        lose.dispose();
        font.dispose();
	}

    public void setUpRound(){
        Collections.shuffle(faces);
        answerIndex = MathUtils.random(8);
        //calculate "good touch" bounds
        switch (answerIndex){
            case 0:
                targetlowx=cxa;
                targetlowy=rya;
                targethighx=cxa+146;
                targethighy=rya+146;
                break;
            case 1:
                targetlowx=cxb;
                targetlowy=rya;
                targethighx=cxb+146;
                targethighy=rya+146;
                break;
            case 2:
                targetlowx=cxc;
                targetlowy=rya;
                targethighx=cxc+146;
                targethighy=rya+146;
                break;
            case 3:
                targetlowx=cxa;
                targetlowy=ryb;
                targethighx=cxa+146;
                targethighy=ryb+146;
                break;
            case 4:
                targetlowx=cxb;
                targetlowy=ryb;
                targethighx=cxb+146;
                targethighy=ryb+146;
                break;
            case 5:
                targetlowx=cxc;
                targetlowy=ryb;
                targethighx=cxc+146;
                targethighy=ryb+146;
                break;
            case 6:
                targetlowx=cxa;
                targetlowy=ryc;
                targethighx=cxa+146;
                targethighy=ryc+146;
                break;
            case 7:
                targetlowx=cxb;
                targetlowy=ryc;
                targethighx=cxb+146;
                targethighy=ryc+146;
                break;
            case 8:
                targetlowx=cxc;
                targetlowy=ryc;
                targethighx=cxc+146;
                targethighy=ryc+146;
                break;
        }
    }

    static class Face {
        Vector2 position = new Vector2();
        TextureRegion image;
        int originalIndex;

        public Face(float x, float y, TextureRegion image, int originalIndex) {
            this.position.x = x;
            this.position.y = y;
            this.image = image;
            this.originalIndex = originalIndex;
        }
    }

    static enum GameState {
        Begin, ShowAnswer, GetAnswer,ShowWin, ShowLose;
    }
}

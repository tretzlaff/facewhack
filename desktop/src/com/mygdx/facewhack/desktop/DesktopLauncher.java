package com.mygdx.facewhack.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.facewhack.MyGdxGameFaceWhack;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Face Whack";
		config.height = 580;
		config.width = 580;
		new LwjglApplication(new MyGdxGameFaceWhack(), config);
	}
}
